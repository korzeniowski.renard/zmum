from sklearn import discriminant_analysis as da
from sklearn import metrics
from sklearn import model_selection
from statsmodels.api import datasets as ds

class LDA:
    def __init__(self):
        self.mean0 = 0
        self.mean1 = 0
        self.cov = 0
        self.fitted = False
        self.pi0 = 0
        self.pi1 = 0

    def fit(self, X, y):
        self.mean0 = np.mean(X[~y], axis=0)
        self.mean1 = np.mean(X[y], axis=0)
        self.cov = np.cov(X.T)
        self.pi0 = np.sum(1 - y) / len(y)
        self.pi1 = np.sum(y) / len(y)
        self.fitted = True

    def predict(self, X):
        if self.fitted:

            preds = []
            for x in X:
                p_xy_0 = sp.stats.multivariate_normal.pdf(x, self.mean0, self.cov)
                p_xy_1 = sp.stats.multivariate_normal.pdf(x, self.mean1, self.cov)

                p_x = p_xy_0 * self.pi0 + p_xy_1 * self.pi1

                p_yx_0 = (p_xy_0 * self.pi0) / p_x
                p_yx_1 = (p_xy_1 * self.pi1) / p_x

                if p_yx_1 > p_yx_0:
                    preds.append(1)
                else:
                    preds.append(0)

            return preds

        else:
            raise Exception('You have to fit model first')


class QDA:
    def __init__(self):
        self.mean0 = 0
        self.mean1 = 0
        self.cov0 = 0
        self.cov1 = 0
        self.fitted = False
        self.pi0 = 0
        self.pi1 = 0

    def fit(self, X, y):
        self.mean0 = np.mean(X[~y], axis=0)
        self.mean1 = np.mean(X[y], axis=0)
        self.cov0 = np.cov(X[~y].T)
        self.cov1 = np.cov(X[y].T)
        self.pi0 = np.sum(1 - y) / len(y)
        self.pi1 = np.sum(y) / len(y)
        self.fitted = True

    def predict(self, X):
        if self.fitted:

            preds = []
            for x in X:
                p_xy_0 = sp.stats.multivariate_normal.pdf(x, self.mean0, self.cov0)
                p_xy_1 = sp.stats.multivariate_normal.pdf(x, self.mean1, self.cov1)

                p_x = p_xy_0 * self.pi0 + p_xy_1 * self.pi1

                p_yx_0 = (p_xy_0 * self.pi0) / p_x
                p_yx_1 = (p_xy_1 * self.pi1) / p_x

                if p_yx_1 > p_yx_0:
                    preds.append(1)
                else:
                    preds.append(0)

            return preds

        else:
            raise Exception('You have to fit model first')


df = ds.get_rdataset(package='ISLR', dataname='Default').data
X = df[['balance', 'income']]
y = df.default.replace({'Yes': 1, 'No': 0}).values

X_train, X_test, y_train, y_test = model_selection.train_test_split(
    X, y, test_size=0.2, random_state=42)

for model in [LDA, QDA]:
    clf = model()
    clf.fit(X_train, y_train)

    y_preds = clf.predict(X_test)
    acc_score = metrics.accuracy_score(y_test, y_preds)
    print(f'{clf.__class__.__name__}: {acc_score}')
