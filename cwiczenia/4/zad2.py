from sklearn import linear_model as lm
from sklearn import model_selection
from statsmodels import api as sm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv('../../data/earthquake.txt', sep=' ')

# sns.scatterplot(x="surface", y="body", hue='popn', data=df)
# plt.show()

df['popn'] = pd.Categorical(df['popn']).codes

df = df.values

y = df[:, :1]
X = df[:, 1:]

train_X, test_X, train_y, test_y = model_selection.train_test_split(X, y)

model = sm.OLS(y, X)

results = model.fit()
print(results.aic)
print(results.bic)

print(results.params)

print(results.tvalues)

print(results.t_test([1, 1]))
