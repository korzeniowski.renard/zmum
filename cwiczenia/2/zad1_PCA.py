import pandas as pd
import numpy as np
from sklearn import decomposition
import matplotlib.pyplot as plt

df = pd.read_csv('USArrests.csv', index_col=0)

df.max()

print(df.var()**1/2)
print(df.var())

standardized_df = (df - df.mean()) / df.var()**(1/2)
cov = standardized_df.cov()

w, v = np.linalg.eig(cov)
pca_df = standardized_df @ v

X = standardized_df.values
pca = decomposition.PCA(4)
pca.fit(X)
pca_X = pca.transform(X)
sk_df = pd.DataFrame(pca_X)
sk_pc = pca.singular_values_
import pdb;pdb.set_trace()
w = sorted(w)
sk_pc = sorted(sk_pc)


ww = [np.sum(w[:i+1])/np.sum(w) for i in range(len(w))]
x = np.linspace(1, 4, 4)
plt.plot(x, ww)

print(ww)

v = [np.sum(sk_pc[:j+1])/np.sum(sk_pc) for j in range(len(sk_pc))]
x = np.linspace(1, 4, 4)
plt.plot(x, v)

print(v)

plt.show()


plt.scatter(pca_df[0], pca_df[1])
plt.scatter(sk_df[0], sk_df[1])
plt.show()