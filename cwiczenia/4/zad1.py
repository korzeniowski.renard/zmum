from sklearn import linear_model as lm
from sklearn import model_selection
from statsmodels import api as sm
from statsmodels.discrete.discrete_model import Logit
# from statsmodels.regression.linear_model import OLS
import pandas as pd
import numpy as np

df = pd.read_csv('http://statweb.stanford.edu/~tibs/ElemStatLearn/datasets/SAheart.data', index_col=0)

cats = pd.get_dummies(df['famhist'])
df = df.drop(columns='famhist')

df = pd.concat([df, cats], axis=1)

print(df.head())

y = df['chd']
X = df.drop(columns='chd')

train_X, test_X, train_y, test_y = model_selection.train_test_split(X, y)

# model = lm.LogisticRegression()
# model.fit(train_X, train_y)
# print(model.coef_)

model = sm.OLS(y, X)

results = model.fit()
print(results.aic)
print(results.bic)

print(results.params)

print(results.tvalues)

print(results.t_test([1]*10))

print(np.exp(results.params))

print(results.conf_int())

r_matrix = np.eye(len(results.params))[1:4]
print(r_matrix)
import pdb;pdb.set_trace()
print(results.wald_test(np.eye(len(results.params))[1:4]))
print(results.wald_test(r_matrix))


