from sklearn import discriminant_analysis as da
from sklearn import metrics
from sklearn import model_selection
import numpy as np

def run_experiment(X, y, str):
    X_train, X_test, y_train, y_test = model_selection.train_test_split(
        X, y, test_size=0.2, random_state=42)

    for model in [da.QuadraticDiscriminantAnalysis, da.LinearDiscriminantAnalysis]:
        clf = model()
        clf.fit(X_train, y_train)

        y_preds = clf.predict(X_test)
        acc_score = metrics.accuracy_score(y_test, y_preds)
        print(f'{str} {clf.__class__.__name__}: {acc_score}')


# a)
G0 = np.random.normal(loc=0, scale=1, size=500)
G1 = np.random.normal(loc=1, scale=1, size=500)
X = np.concatenate([G0, G1])
X = np.stack([X, X])
X = np.moveaxis(X, 1, 0)

y = np.array(500 * [0] + 500 * [1])

run_experiment(
    X=X,
    y=y,
    str='LDA constraints fulfilled'
)

# b)
cov = np.array([[1.0, 0.8], [0.8, 1.0]])
G0 = np.random.multivariate_normal(mean=[0, 0], cov=cov, size=500)
G1 = np.random.multivariate_normal(mean=[1, 1], cov=cov, size=500)
X = np.concatenate([G0, G1])


run_experiment(
    X=X,
    y=y,
    str='LDA constraints not fulfilled'
)
