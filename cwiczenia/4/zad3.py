from sklearn import linear_model as lm
from sklearn import model_selection
import matplotlib.pyplot as plt
import numpy as np

losses = []
losses_norm = []
ds_sizes = np.arange(50, 350, 50)

for N in ds_sizes:

    x1 = np.random.normal(size=N)
    x2 = np.random.normal(size=N)
    beta_0 = 0.5
    beta_1 = 1
    beta_2 = 1
    beta = np.array([beta_0, beta_1, beta_2])

    data = np.column_stack([x1, x2])

    p = 1 / (1 + np.exp(-(beta_0 + beta_1*x1 + beta_2*x2)))

    y = np.random.binomial(1, p=p)

    model = lm.LogisticRegression()
    model.fit(data, y)

    beta_hat = np.insert(model.coef_, 0, model.intercept_)

    mse = np.sum((np.linalg.norm(beta - beta_hat, 2))**2)/3
    losses.append(mse)

plt.plot(ds_sizes, losses)
plt.show()
