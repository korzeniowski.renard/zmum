from sklearn import linear_model as lm
from sklearn import model_selection
from statsmodels import api as sm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv('../../data/pacjenci.csv', sep=';')

y = df['zgon']
data = df.drop(columns='zgon')
model1 = sm.OLS(y, data)
results1 = model1.fit()

# standard model
summary = results1.summary()
print(summary)
imp_cols = (results1.pvalues < 0.05)
imp_cols.name = 'p_vals'

imp_cols = imp_cols.reset_index()
imp_cols = imp_cols[imp_cols['p_vals']]['index']

# only imp columns model
data = data[imp_cols]
model2 = sm.OLS(y, data)
results2 = model2.fit()

summary = results2.summary()
print(summary)


from statsmodels.stats.anova import anova_lm
anova_results = anova_lm(results1, results2)
print(anova_results)

# its not really the case that prob is growing with exp(params)
prob_per_coef = results2.params
print(prob_per_coef)

obs = {'wstrzas': [0], 'alkohol': [1], 'zawal': [1]}
df_single_sample = pd.DataFrame.from_dict(obs)

pred = results2.predict(df_single_sample)
print(pred)
