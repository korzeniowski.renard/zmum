import numpy as np
from scipy.stats import norm, gaussian_kde
import statsmodels.api as sm

p = 0.1
y = np.random.binomial(1, p, 100)

X1 = np.random.normal(loc=10, scale=1, size=100)
X2 = np.random.normal(loc=5, scale=1, size=900)

X = np.concatenate([X1, X2])

X11 = norm.pdf(y, 5, 1)
X22 = norm.pdf(1-y, 10, 1)

XX = X11 * 0.9 + X22 * 0.1

print(X.mean(), X.shape)
print(XX.mean(), X.shape)
print(y)

import matplotlib.pyplot as plt


kernel = gaussian_kde(X)
Z = np.reshape(kernel(X).T, X.shape)

from sklearn.neighbors import KernelDensity

XXX = X.reshape(-1, 1)
kde = KernelDensity(kernel='gaussian', bandwidth=0.2).fit(XXX)
log_dens = kde.score_samples(XXX)
plt.scatter(XXX[:, 0], np.exp(log_dens))

plt.hist(X, density=True)
plt.plot(Y)
plt.show()